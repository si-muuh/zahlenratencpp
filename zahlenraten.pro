TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    zufallszahl.cpp \
    schwierigkeit.cpp \
    anleitung.cpp \
    statistik.cpp

include(deployment.pri)
qtcAddDeployment()

