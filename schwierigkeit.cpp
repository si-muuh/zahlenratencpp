#include <iostream>

using namespace std;

int schwierigkeit(){

  int anzahlVersuche = 0;
  int eingabeBenutzer = 0;
  bool richtigeAuswahl = false;

  cout << "| Mit wievielen Versuchen m\224chten"  << endl;
  cout << "| Sie spielen? W\204hle "<< endl;
  cout << "| 1  -> 5 Versuche (Anf\204nger)" << endl;
  cout << "| 2  -> 4 Versuche (Na ja ...)" << endl;
  cout << "| 3  -> 3 Versuche (Profi)" << endl;
  cout << "| 4  -> 1 Versuch  (Unrealistisch)" << endl;
  cout << "+--------------------------------+" << endl;

  do{
      cout << "| Deine Eingabe > ";
      fflush(stdin);
      scanf("%d",&eingabeBenutzer);
    //}while (eingabeBenutzer < 1 || eingabeBenutzer > 4);

  switch (eingabeBenutzer){
    case 1:
      anzahlVersuche = 5;
      cout << "Sie haben die Stufe \"Anf\204nger\" gew\204hlt" << endl;
      richtigeAuswahl = true;
      break;
    case 2:
      anzahlVersuche = 4;
      cout << "Sie haben die Stufe \"Na ja ...\" gew\204hlt" << endl;
      richtigeAuswahl = true;
      break;
    case 3:
      anzahlVersuche = 3;
      cout << "Sie haben die Stufe \"Profi\" gew\204hlt" << endl;
      richtigeAuswahl = true;
      break;
    case 4:
      anzahlVersuche = 1;
      cout << "Sie haben die Stufe \"Unrealistisch\" gew\204hlt, viel erfolg!" << endl;
      richtigeAuswahl = true;
      break;
    default:
      cout << "Falsche Auswahl, bitte wiederholen!" << endl;
      richtigeAuswahl = false;
      break;
    }
  }while(richtigeAuswahl == false);

  return anzahlVersuche;
}
