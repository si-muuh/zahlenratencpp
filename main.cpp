#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>
#define MAX 30

using namespace std;

extern int zufallszahl();
extern int schwierigkeit();
extern anleitung();
extern statistik(int anzahlSpiele, int anzahlSiege);


void head(){
  cout << "+----------------------------------------+" << endl;
  cout << "| Das Zahleratenspiel v0.2" << endl;
  cout << "| Programmierer: si-muuh" << endl;
  cout << "| Git: https://bitbucket.org/si-muuh" << endl;
  cout << "+----------------------------------------+" << endl;
}

string benutzerName(void){
  string benutzername = "leer";
  cout << "| Gib einen Name an > ";
  cin >> benutzername;
  return benutzername;
}


int main()
{
  ////////////////////////////////////////////////////
  // Variablen die waehrend des Spiel benötigt werden
  ////////////////////////////////////////////////////

  int eingabeZahlBenutzer = 0; // Zahl die vom Benutzer waehrend des Spiels eingegeben wird
  bool erraten = false; // Wurde die richtige Zahl erraten?
  int versucheVerwendet = 0; // Anzahl versuche die der Spieler bereits benoetigt hat
  int spielWiederholen = 0; // Will der Benutzer das Spiel wiederholen?
  int anzahlSiege = 0; // Fuer die Statistik: Anzahl Siege
  int anzahlSpiele = 0; // Fuer die Statistik: Anzahl Spiele
  string benutzername; // Der vom Benutzer gewaehlten Name


  do{ // Die "Spiel wiederholen" Schleife

      // Hier nur ein Bildschirm loeschen wenn bereits einmal gespielt wurde
      if (anzahlSpiele > 0) system("cls");

  //Aufrufen der Titelseite
  head();

  // Anleitung nur einmal anzeigen
  if (anzahlSpiele < 1)  anleitung();

  // Benutzername nur einmal abfragen
  if (anzahlSpiele < 1)benutzername = benutzerName();

  //Je nach auswahl vom Benutzer die anzahl der Versuche
  int maxVersucheBenutzer = schwierigkeit();
  //Generieren und Variable füllen mit einer Zufallszahl
  int computerZufall = zufallszahl();

  //////////////////////////////////////////////
  // Das Spiel startet hier
  //////////////////////////////////////////////

  do{ // Die "solange versuchen bis alle Versuche augebraucht" sind Schleife
      do{ // Benutzer abfangen bei falscher Eingabe.
          cout << benutzername << ", gib eine Zahl ein > ";
          cin >> eingabeZahlBenutzer;
          //ToDO -> Falsche Eingabe abfangen.
        }while(eingabeZahlBenutzer < 1 || eingabeZahlBenutzer > 20);

        if (eingabeZahlBenutzer < computerZufall){
            cout << "| Deine Zahl ist zu tief" << endl;
          }
        if (eingabeZahlBenutzer > computerZufall){
            cout << "| Deine Zahl ist zu hoch" << endl;
          }
        if (eingabeZahlBenutzer == computerZufall){
            erraten = true;
            versucheVerwendet++;
            break; //frühzeitiges raus aus der Schleife
          }
        //Anzahl Versuche hochzaehlen
        versucheVerwendet++;

    //Schleife solange durchmachen bis die versuche aufgebraucht sind.
    }while (versucheVerwendet < maxVersucheBenutzer);

  //////////////////////////////////////////////
  // Das Spiel ist hier fertig
  //////////////////////////////////////////////

  //Richtige Zahl erraten?
  switch (erraten){

    // Zahl erraten
  case true:
      printf("| Sie haben die richtige Zahl \"%d\" mit %d Versuchen erraten. Gratuliere!\n",computerZufall,versucheVerwendet);
      anzahlSiege++;
      erraten = false; // zurücksetzten der Variable
    break;

    // Zahl nicht erraten
  case false:
    printf("| Sie haben die richtige Zahl \"%d\" leider nicht erraten, FLASCHE!\n",computerZufall);
    break;
}
  anzahlSpiele++;
  cout << "| " << benutzername << ", m\224chten Sie ein weiteres Spiel spielen?" << endl;
  cout << "| 1 -> Ja | 2 -> Nein" << endl;
  cout << "| Deine Eingabe > ";
      cin >> spielWiederholen;

    //Zurücksetzten der verwendeten Versuche
    versucheVerwendet = 0;

// Solange wiederholen bis "2" gewaehlt wird.
}while (spielWiederholen != 2);

  //Nach abbruch die Statistik aufrufen
  statistik(anzahlSpiele,anzahlSiege);
}

